package com.ctsi.forum.util;


import javax.ws.rs.core.Response;

import org.apache.cxf.jaxrs.client.WebClient;

public class WebClientUtil {
	public static WebClient getWebClientDefaultContentType(String url, String acceptType) {
		try {
			WebClient client = WebClient.create(url);
			client.accept(acceptType);
			return client;
		} catch (Exception e) {
			throw new IllegalArgumentException("获取连接失败", e);
		}
	}
	public static WebClient getWebClient(String url, String contentType) {
		try {
			WebClient client = WebClient.create(url);
			client.header("Content-Type", contentType);
			return client;
		} catch (Exception e) {
			throw new IllegalArgumentException("获取连接失败", e);
		}
	}

	public static WebClient getWebClient(String url, String contentType,
			String acceptType) {
		WebClient client = getWebClient(url,contentType);
		try {
			client.accept(acceptType);
			return client;
		} catch (Exception e) {
			throw new IllegalArgumentException("获取连接失败", e);
		}
	}

	public static void closeWebClient(WebClient client) {
		if (client != null) {
			try {
				client.close();
			} catch (Exception e) {
				throw new IllegalArgumentException("关闭连接失败", e);
			}
		}
	}

	public static Response post(WebClient client, Object params) {
		try {
			return  client.post(params);
		} catch (Exception e) {
			throw new IllegalArgumentException("请求超时", e);
		}
	}
	public static Response get(WebClient client) {
		try {
			return client.get();
		} catch (Exception e) {
			throw new IllegalArgumentException("请求超时", e);
		}
	}

	public static Response post(String url, String contentType,
			String acceptType, Object params) {
		WebClient client = getWebClient(url, contentType, acceptType);
		return post(client, params);
	}

	public static Response post(String url, String contentType, Object params) {
		WebClient client = getWebClient(url, contentType);
		return post(client, params);
	}
	public static Response get(String url,String acceptType){
		WebClient client = getWebClientDefaultContentType(url,acceptType);
		return get(client);
	}
}
