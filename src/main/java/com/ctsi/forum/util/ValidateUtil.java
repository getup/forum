package com.ctsi.forum.util;

import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;

public final class ValidateUtil {

	private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

	public static <T extends Object> boolean isValid(T obj) {
		Set<ConstraintViolation<T>> cc = validator.validate(obj);
		if (cc.isEmpty()) {
			return true;
		}
		Set<String> ms = new HashSet<String>();
		for (ConstraintViolation<T> co : cc) {
			ms.add(co.getMessage());
		}
		throw new IllegalArgumentException(StringUtils.join(ms, ','));
	}

}
