package com.ctsi.forum.util;


import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * 
* @ClassName: DateUtil 
* @Description: TODO(日期类相互转换工具类) 
* @author  jerome.jiang@pp.plateno.cc
* @date 2014年9月18日 下午3:50:18 
*
 */
public class DateUtil {
	
	/**
	 * 计算指定日期的偏移日期
	 * @param selectDate
	 * @param offsetDay
	 * @return
	 */
	public static Date calDate(Date selectDate,int offsetDay){
		
		Calendar calendar=Calendar.getInstance();   
		calendar.setTime(selectDate);		
		calendar.set(Calendar.DAY_OF_YEAR,calendar.get(Calendar.DAY_OF_YEAR) + offsetDay);  
		return calendar.getTime();
		
	}
	
	/**
	 * 获取当前日期的当月第一天
	 * @param      date
	 * @return     
	 * @exception  
	 */
	public static Date getFirstDay(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(GregorianCalendar.DAY_OF_MONTH, 1);  
        return cal.getTime();
	}
	
	/**
	 * 获取当前日期的当月最后一天
	 * @param      date
	 * @return     
	 * @exception  
	 */
	public static Date getLastDay(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set( Calendar.DATE, 1 );  
        cal.roll(Calendar.DATE, - 1 );
        return cal.getTime();
	}
	

	/**
	 * 获取当前日期的下一天
	 * @param      date
	 * @return     
	 * @exception  
	 */
	public static Date getNextDate(Date date){
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.HOUR, 24);
		return cal.getTime();
	}
	
	/**
	 * 获取当前日期的当月的所有日期
	 * @param      date
	 * @return     
	 * @exception  
	 */
	public static List<Date> getNextDateList(Date date){
		List<Date> list = new ArrayList<Date>();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		Date whenDate = date;
		Date endDate = getLastDay(date);
		while(whenDate.before(endDate)||whenDate.equals(endDate)){
			list.add(whenDate);
			whenDate = getNextDate(whenDate);
		}
		return list;
	}
	
	/***
	 *  转换成date类型格式
	 * @param dataStr
	 * @return
	 * @throws ParseException
	 */
	public static Date stringToDate(String dataStr) throws ParseException{		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return df.parse(dataStr);
	}	
	
	public static Date StringTodate2(String dataStr) throws ParseException{		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		return df.parse(dataStr);
	}	
	

	public static Date StringTodate3(String dataStr) throws ParseException{		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		return df.parse(dataStr);
	}
	
	public static Date StringTodate4(String dataStr){		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return df.parse(dataStr);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Date StringTodateYMD(String dataStr) throws Exception{		
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try {
			return df.parse(dataStr);
		} catch (ParseException e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}	
	
	public static String datetoString2(Date date){
		if(date != null){
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			return df.format(date);
		}
		return "";
	}
	
	/**
	 * date类型转化为指定的字符串类型格式
	 * @param date
	 * @return
	 */
	public static String dateToString(Date date){
		if(date != null){
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			return df.format(date);
		}
		return "";
	}
	
	/**
	 * date类型转化为指定的字符串类型格式
	 * @param date
	 * @return
	 */
	public static String dateToString3(Date date){
		if(date != null){
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return df.format(date);
		}
		return "";
	}
	
	/**
	 * date类型转化为指定的字符串类型格式
	 * @param date
	 * @return
	 */
	public static String dateToString2(Date date){
		if(date != null){
			SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
			return df.format(date);
		}
		return "";
	}
	
	/**
	 * date类型转化为指定的字符串类型格式
	 * @param date
	 * @return
	 */
	public static Date dateToDateYYYYMMDD(Date date){
		if(date != null){
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			try {
				return df.parse(df.format(date));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	/**
	 * date类型转化为指定的字符串类型格式
	 * @param date
	 * @return
	 */
	public static Date dateToDateHHMMDD(Date date){
		if(date != null){
			SimpleDateFormat df = new SimpleDateFormat("HH:mm:dd");
			try {
				return df.parse(df.format(date));
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	
	
	public static String dateToStringYYYYMMDD(Date date){
		if(date != null){
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			return df.format(date);
		}
		return "";
	}
	
	
	

	public static String dateToStringYYYY(Date date){
		if(date != null){
			SimpleDateFormat df = new SimpleDateFormat("yyyy");
			return df.format(date);
		}
		return "";
	}
	
	/**
	 * 查询2个日期是否同1天
	 * @param t
	 * @param t2
	 * @return
	 */
	public static boolean IsSameDay(Timestamp t,Timestamp t2){
		Calendar cal = Calendar.getInstance();
		cal.setTime(t);
		int y = cal.get(Calendar.YEAR);
		int d = cal.get(Calendar.DAY_OF_YEAR);
		
		cal.setTime(t2);
		int y2 = cal.get(Calendar.YEAR);
		int d2 = cal.get(Calendar.DAY_OF_YEAR);
		
		return (y==y2) && (d==d2);
	}
	
	/**获取当前时间的Timestamp值*/
	public static Timestamp NowTimestamp(){
		return new Timestamp(new Date().getTime());
	}
	
	
	/**
	 * 获取当前时间(注意:返回的是String 已经格式化好的格式)
	 * @return
	 */
	public static String getNowTime(){
		Date nowDate = new Date();
		Calendar now = Calendar.getInstance();
		now.setTime(nowDate);
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    String str = formatter.format(now.getTime());
		return str;
	}
	
	/**
	 * 获取当前时间(注意:返回的是String 已经格式化好的格式)
	 * @return
	 */
	public static String getNowTimeFormat(){
		Date nowDate = new Date();
		Calendar now = Calendar.getInstance();
		now.setTime(nowDate);
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
	    String str = formatter.format(now.getTime());
		return str;
	}
	
	/**
	 * 获取今天的开始时间(注意:返回的是String 已经格式化好的格式)
	 * @return
	 */
	public static Date getTodayStartTime(Date date){
		 Calendar calendar = Calendar.getInstance();
		    calendar.setTime(date);
		    calendar.set(Calendar.HOUR_OF_DAY, 0);
		    calendar.set(Calendar.MINUTE, 0);
		    calendar.set(Calendar.SECOND, 0);
		    
		  return calendar.getTime();
	}
	
	/**
	 * 获取今天的开始时间(注意:返回的是String 已经格式化好的格式)
	 * @return
	 */
	public static Date getTodayEndTime(Date date){
		 Calendar calendar = Calendar.getInstance();
		    calendar.setTime(date);
		    calendar.set(Calendar.HOUR_OF_DAY, 0);
		    calendar.set(Calendar.MINUTE, 0);
		    calendar.set(Calendar.SECOND, 0);
		    calendar.add(Calendar.DAY_OF_MONTH, 1);
		    calendar.add(Calendar.SECOND, -1);
		  return calendar.getTime();
	}
	
	
	/**
	 * 获取当前时间(注意:返回的是String 已经格式化好的格式)
	 * @return
	 */
	public static String getNowTimeFormat(String format){
		Date nowDate = new Date();
		Calendar now = Calendar.getInstance();
		now.setTime(nowDate);
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		String str = formatter.format(now.getTime());
		return str;
	}

	public static String getNowTimeFormat(Date nowDate){
		if (nowDate==null) nowDate = new Date();
		Calendar now = Calendar.getInstance();
		now.setTime(nowDate);
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
	    String str = formatter.format(now.getTime());
		return str;
	}
	
	public static long getTimeInMillis(String sDate, String eDate){
		Timestamp sd = Timestamp.valueOf(sDate);
		Timestamp ed = Timestamp.valueOf(eDate);
		Calendar calendar = Calendar.getInstance(); 
		calendar.setTime(sd); 
		long timethis = calendar.getTimeInMillis();  
		
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(ed); 
		long timeend = calendar2.getTimeInMillis(); 
		long thedaymillis = timeend-timethis; 
		return thedaymillis;
	}
	
	/**
	 *   HH:mm:ss格式化的时间
	 * @param dTime
	 * @return
	 */
	public static String formatTime(String dTime) {
		String dateTime = "";
		if(dTime != null && !"".equals(dTime)) {
			Timestamp t = Timestamp.valueOf(dTime);
			SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd");
			dateTime = formatter.format(t);
		}
		return dateTime;
	}
	
	/**
	 *  强制转化为时间格式
	 * @param strDate
	 * @param pattern
	 * @return
	 * @throws ParseException
	 */
	public static Date parses(String   strDate,   String   pattern)   throws   ParseException   {   
		  return new SimpleDateFormat(pattern).parse(strDate);   
	} 
	
	/**
	 * 当前日期是第几周
	 * @return
	 */
	public static String getWeekOfYear(){
		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		String week = calendar.get(Calendar.WEEK_OF_YEAR)+"";
		return week; 
	}
	
	/**
	 * 返回毫秒
	 * @param sDate
	 * @param eDate
	 * @return
	 */
	public static String getTimeInMillis(Date sDate, Date eDate){
		Calendar calendar = Calendar.getInstance(); 
		calendar.setTime(sDate); 
		long timethis = calendar.getTimeInMillis();  
		
		Calendar calendar2 = Calendar.getInstance();
		calendar2.setTime(eDate); 
		long timeend = calendar2.getTimeInMillis(); 
		long thedaymillis = timeend-timethis; 
		return thedaymillis < 1000 ? thedaymillis + "毫秒!" : (thedaymillis/1000) + "秒钟!";
	}
	
	
	/**
	 * 获取第i月之后的时间
	 * @param ts
	 * @param i
	 * @return
	 */
	public static String getNextDate(String ts, int i){
		Calendar now = Calendar.getInstance();
		Timestamp t = Timestamp.valueOf(ts + " 00:00:00.000");
		now.setTime(t);
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	    now.add(Calendar.DAY_OF_MONTH, +(i)); 
		String dt = formatter.format(now.getTime());
		return dt;
	}
	
	/**
	 * 获取第i分钟之后的时间
	 * @param ts
	 * @param i
	 * @return
	 */
	public static String getNextTime(String ts, int i){
		Calendar now = Calendar.getInstance();
		Timestamp t = Timestamp.valueOf(ts);
		now.setTime(t);
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    now.add(Calendar.MINUTE, +(i)); 
		String dt = formatter.format(now.getTime());
		return dt;
	}
	

	/***
	 * 取Unix时间戳
	 * @param dateTime
	 * @return
	 */
	public static long getUnixTime(String dateTime) {
		Date date1 = null;
		Date date2 = null;
		try {
			date1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dateTime);
		    date2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("1970-01-01 08:00:00");
		} catch (ParseException e) {
			e.printStackTrace();
		}
        long l = (date1.getTime() - date2.getTime())/1000;
        return l;
	}
	
	/**
	 * 计算两个日期之间的相隔的年、月、日。注意：只有计算相隔天数是准确的，相隔年和月都是 近似值，按一年365天，一月30天计算，忽略闰年和闰月的差别。
	 * 
	 * @param datepart
	 *            两位的格式字符串，yy表示年，MM表示月，dd表示日
	 * @param startdate
	 *            开始日期
	 * @param enddate
	 *            结束日期
	 * @return double 如果enddate>startdate，返回一个大于0的实数，否则返回一个小于等于0的实数
	 */
	public static double dateDiff(String datepart, Date startdate, Date enddate) {
		if (datepart == null || datepart.equals("")) {
			throw new IllegalArgumentException("DateUtil.dateDiff()方法非法参数值："
					+ datepart);
		}

		double days = (double) (enddate.getTime() - startdate.getTime())
				/ (60 * 60 * 24 * 1000);

		if (datepart.equals("yy")) {
			days = days / 365;
		} else if (datepart.equals("MM")) {
			days = days / 30;
		} else if (datepart.equals("dd")) {
			return days;
		} else {
			throw new IllegalArgumentException("DateUtil.dateDiff()方法非法参数值："
					+ datepart);
		}
		return days;
	}
	
	
	/**
    * 把日期对象加减年、月、日后得到新的日期对象
    * @param depart yy-年、MM-月、dd-日
    * @param number 加减因子
    * @param date 需要加减年、月、日的日期对象
    * @return Date 新的日期对象
    */
   public static Date addDate(String datepart, int number, Date date)
   {
      Calendar cal = Calendar.getInstance();
      cal.setTime(date);
      if(datepart.equals("yy")) {
         cal.add(Calendar.YEAR, number);
      } else if(datepart.equals("MM")) {
         cal.add(Calendar.MONTH, number);
      } else if(datepart.equals("dd")) {
         cal.add(Calendar.DATE, number);
      } else {
         throw new IllegalArgumentException("DateUtil.addDate()方法非法参数值：" +
                                            datepart);
      }

      return cal.getTime();
   }

}
