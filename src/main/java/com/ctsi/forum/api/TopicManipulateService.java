package com.ctsi.forum.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.ctsi.forum.constant.MediaType;
import com.ctsi.forum.entity.ResponseData;
import com.ctsi.forum.entity.TopicDeleteParam;
import com.ctsi.forum.entity.TopicParam;
import com.ctsi.forum.entity.TopicSearchParam;
import com.ctsi.forum.entity.UpdateParam;

@Path("/topic")
public interface TopicManipulateService {
	@POST
	@Path("/add")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public ResponseData addTopic(TopicParam param);
	
	@POST
	@Path("/delete")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public ResponseData deleteTopic(TopicDeleteParam param);
	
	@POST
	@Path("/search")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public ResponseData searchTopic(TopicSearchParam param);
	
	@POST
	@Path("/update")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public ResponseData updateTopic(UpdateParam param);

}
