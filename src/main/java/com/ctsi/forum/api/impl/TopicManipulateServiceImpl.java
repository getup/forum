package com.ctsi.forum.api.impl;


import org.springframework.beans.factory.annotation.Autowired;

import com.ctsi.forum.api.TopicManipulateService;
import com.ctsi.forum.constant.ServerCode;
import com.ctsi.forum.entity.ResponseData;
import com.ctsi.forum.entity.TopicData;
import com.ctsi.forum.entity.TopicDeleteParam;
import com.ctsi.forum.entity.TopicParam;
import com.ctsi.forum.entity.TopicSearchParam;
import com.ctsi.forum.entity.UpdateParam;
import com.ctsi.forum.service.TopicService;

public class TopicManipulateServiceImpl extends BaseJaxrsService implements
 TopicManipulateService {
	@Autowired
	TopicService topicService;
	
	@Override
	public ResponseData addTopic(TopicParam param) {
		// TODO Auto-generated method stub
		logger.info("【实战项目】创建事项,topicName[" + param.getTopicName()+ "],completeTime["+ param.getCompleteTime()+  "],planTime["+ param.getPlanTime()+"]");
		ResponseData data = createResponseData(param);
		if (data.getCode() != ServerCode.SUCESS.code()) {
			return data;
		}
		try {
			TopicData topicData=topicService.addTopic(param);
			data.setData(topicData);
			logger.info("【实战项目】创建事项,topicName[" + param.getTopicName() + "],completeTime["
				+ param.getCompleteTime() + "],返回:Code[" + data.getCode() + "],Message["
				+ data.getMessage()+"]");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			bizlogger.error("【实战项目】创建事项失败，系统异常 topicName[" + param.getTopicName() + "]", e);
			data.setCode(ServerCode.SYSTEM_ERROR.code());
			data.setMessage(ServerCode.SYSTEM_ERROR.message());
			data.setData(null);
			return data;
		}
		return null;
		
	}
	@Override
	public ResponseData deleteTopic(TopicDeleteParam param) {
		// TODO Auto-generated method stub
		logger.info("【实战项目】删除事项,topicId[" +param.getTopicId() + "]");
		ResponseData data = createResponseData(param);
		if (data.getCode() != ServerCode.SUCESS.code()) {
			return data;
		}
		try {
			TopicData topicData=topicService.deleteTopic(param);
			data.setData(topicData);
			logger.info("【实战项目】删除事项,topicId[" + param.getTopicId() + "]");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			bizlogger.error("【实战项目】删除事项失败，系统异常 topicId" + param.getTopicId() + "]", e);
			data.setCode(ServerCode.SYSTEM_ERROR.code());
			data.setMessage(ServerCode.SYSTEM_ERROR.message());
			data.setData(null);
			return data;
		}
		return null;
	}
	
	@Override
	public ResponseData searchTopic(TopicSearchParam param) {
		// TODO Auto-generated method stub
		logger.info("【实战项目】查询事项,topicId[" +param.getTopicName()+ "]");
		ResponseData data = createResponseData(param);
		if (data.getCode() != ServerCode.SUCESS.code()) {
			return data;
		}
		try {
			TopicData topicData=topicService.searchTopic(param);
			data.setData(topicData);
			logger.info("【实战项目】查询事项,topicId[" + param.getTopicName() + "]");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			bizlogger.error("【实战项目】删除事项失败，系统异常 topicId" + param.getTopicName() + "]", e);
			data.setCode(ServerCode.SYSTEM_ERROR.code());
			data.setMessage(ServerCode.SYSTEM_ERROR.message());
			data.setData(null);
			return data;
		}
		return null;
	}
	
	
	
	@Override
	public ResponseData updateTopic(UpdateParam param) {
			// TODO Auto-generated method stub
			logger.info("【实战项目】更新事项,topicId[" +param.getTopicName()+ "]");
			ResponseData data = createResponseData(param);
			if (data.getCode() != ServerCode.SUCESS.code()) {
				return data;
			}
			try {
				TopicData topicData=topicService.updateTopic(param);
				data.setData(topicData);
				logger.info("【实战项目】更新事项,topicId[" + param.getTopicName() + "]");

			} catch (Exception e) {
				// TODO Auto-generated catch block
				bizlogger.error("【实战项目】更新事项失败，系统异常 topicId" + param.getTopicName() + "]", e);
				data.setCode(ServerCode.SYSTEM_ERROR.code());
				data.setMessage(ServerCode.SYSTEM_ERROR.message());
				data.setData(null);
				return data;
			}
			return null;
		}
	

}
