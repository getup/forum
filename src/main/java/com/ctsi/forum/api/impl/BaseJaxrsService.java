package com.ctsi.forum.api.impl;

import org.apache.log4j.Logger;

import com.ctsi.forum.constant.AppMap;
import com.ctsi.forum.constant.ServerCode;
import com.ctsi.forum.entity.App;
import com.ctsi.forum.entity.Auth;
import com.ctsi.forum.entity.RequestParam;
import com.ctsi.forum.entity.ResponseData;
import com.ctsi.forum.util.ValidateUtil;


public class BaseJaxrsService {
	protected static Logger logger = Logger.getLogger(BaseJaxrsService.class);
	protected Logger bizlogger = Logger.getLogger(this.getClass());

	private boolean checkAuth(RequestParam param) {
		Auth auth=param.getAuth();
		try{
			if(auth==null){
				logger.info("【实战项目】权限校验失败,未传入鉴权信息");
				return false;
			}
			App app=AppMap.getApp(auth.getApp());
			if(app==null){
				logger.info("【实战项目】权限校验失败,鉴权信息无效，app["+auth.getApp()+"],timestamp["+auth.getTimestamp()+"],token["+auth.getToken()+"]");
				return false;
			}
			if(!auth.isValid(app.getSecret())){
				logger.info("【实战项目】权限校验失败,鉴权信息无效，app["+auth.getApp()+"],timestamp["+auth.getTimestamp()+"],token["+auth.getToken()+"]");
				return false;
			}
			return true;
		}catch(Exception e){
			logger.error("【实战项目】权限校验异常",e);
			return false;
		}
	}
	public ResponseData createResponseData(RequestParam param){
		ResponseData data=new ResponseData();
		if(!checkAuth(param)){
			data.setCode(ServerCode.AUTH_FAILED.code());
			data.setMessage(ServerCode.AUTH_FAILED.message());
			return data;
		}
		try{
			if(!ValidateUtil.isValid(param)){
				logger.info("【实战项目】参数校验失败");
				data.setCode(ServerCode.OTHER_ERROR.code());
				data.setMessage("参数校验失败");
				return data;
			}
		}catch(Exception e){
			logger.error("【实战项目】参数校验失败",e);
			data.setCode(ServerCode.OTHER_ERROR.code());
			data.setMessage("参数校验失败:["+e.getMessage()+"]");
			return data;
		}
		data.setCode(ServerCode.SUCESS.code());
		data.setMessage(ServerCode.SUCESS.message());
		return data;
	}
}
