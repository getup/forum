package com.ctsi.forum.interceptor;

import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import javax.xml.transform.stream.StreamSource;

import org.apache.cxf.common.util.StringUtils;
import org.apache.cxf.endpoint.Endpoint;
import org.apache.cxf.io.CachedOutputStream;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.service.model.EndpointInfo;
import org.apache.cxf.staxutils.PrettyPrintXMLStreamWriter;
import org.apache.cxf.staxutils.StaxUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public abstract class AbstractLog4jLoggingInterceptor extends AbstractPhaseInterceptor<Message> {
	protected static final String BINARY_CONTENT_MESSAGE = "--- Binary Content ---";
	private static final List<String> BINARY_CONTENT_MEDIA_TYPES;
	static {
		BINARY_CONTENT_MEDIA_TYPES = new ArrayList<String>();
		BINARY_CONTENT_MEDIA_TYPES.add("application/octet-stream");
		BINARY_CONTENT_MEDIA_TYPES.add("image/png");
		BINARY_CONTENT_MEDIA_TYPES.add("image/jpeg");
		BINARY_CONTENT_MEDIA_TYPES.add("image/gif");
	}

	protected int limit = 100 * 1024;
	protected long threshold = -1;
	protected PrintWriter writer;
	protected boolean prettyLogging;
	private boolean showBinaryContent;

	public AbstractLog4jLoggingInterceptor(String phase) {
		super(phase);
	}

	public AbstractLog4jLoggingInterceptor(String id, String phase) {
		super(id, phase);
	}

	protected abstract Logger getLogger();

	protected Logger getMessageLogger(Message message) {
		Endpoint ep = message.getExchange().getEndpoint();
		if (ep == null || ep.getEndpointInfo() == null) {
			return getLogger();
		}
		EndpointInfo endpoint = ep.getEndpointInfo();
		if (endpoint.getService() == null) {
			return getLogger();
		}
		Logger logger = endpoint.getProperty("MessageLogger", Logger.class);
		if (logger == null) {
			logger = Logger.getLogger(this.getClass());
			logger.setLevel(Level.INFO);
			endpoint.setProperty("MessageLogger", logger);
		}
		return logger;
	}

	public void setOutputLocation(String s) {
		if (s == null || "<logger>".equals(s)) {
			writer = null;
		} else if ("<stdout>".equals(s)) {
			writer = new PrintWriter(System.out, true);
		} else if ("<stderr>".equals(s)) {
			writer = new PrintWriter(System.err, true);
		} else {
			try {
				URI uri = new URI(s);
				File file = new File(uri);
				writer = new PrintWriter(new FileWriter(file, true), true);
			} catch (Exception ex) {
				getLogger().log(Level.WARN, "Error configuring log location " + s, ex);
			}
		}
	}

	public void setPrintWriter(PrintWriter w) {
		writer = w;
	}

	public PrintWriter getPrintWriter() {
		return writer;
	}

	public void setLimit(int lim) {
		limit = lim;
	}

	public int getLimit() {
		return limit;
	}

	public void setPrettyLogging(boolean flag) {
		prettyLogging = flag;
	}

	public boolean isPrettyLogging() {
		return prettyLogging;
	}

	public void setInMemThreshold(long t) {
		threshold = t;
	}

	public long getInMemThreshold() {
		return threshold;
	}

	protected void writePayload(StringBuilder builder, CachedOutputStream cos, String encoding, String contentType)
			throws Exception {
		// Just transform the XML message when the cos has content
		if (isPrettyLogging()
				&& (contentType != null && contentType.indexOf("xml") >= 0 && contentType.toLowerCase().indexOf(
						"multipart/related") < 0) && cos.size() > 0) {

			StringWriter swriter = new StringWriter();
			XMLStreamWriter xwriter = StaxUtils.createXMLStreamWriter(swriter);
			xwriter = new PrettyPrintXMLStreamWriter(xwriter, 2);
			InputStream in = cos.getInputStream();
			try {
				StaxUtils.copy(new StreamSource(in), xwriter);
			} catch (XMLStreamException xse) {
				// ignore
			} finally {
				try {
					xwriter.flush();
					xwriter.close();
				} catch (XMLStreamException xse2) {
					// ignore
				}
				in.close();
			}

			String result = swriter.toString();
			if (result.length() < limit || limit == -1) {
				builder.append(swriter.toString());
			} else {
				builder.append(swriter.toString().substring(0, limit));
			}

		} else {
			if (StringUtils.isEmpty(encoding)) {
				cos.writeCacheTo(builder, limit);
			} else {
				cos.writeCacheTo(builder, encoding, limit);
			}
		}
	}

	protected void writePayload(StringBuilder builder, StringWriter stringWriter, String contentType) throws Exception {
		// Just transform the XML message when the cos has content
		if (isPrettyLogging() && contentType != null && contentType.indexOf("xml") >= 0
				&& stringWriter.getBuffer().length() > 0) {

			StringWriter swriter = new StringWriter();
			XMLStreamWriter xwriter = StaxUtils.createXMLStreamWriter(swriter);
			xwriter = new PrettyPrintXMLStreamWriter(xwriter, 2);
			StaxUtils.copy(new StreamSource(new StringReader(stringWriter.getBuffer().toString())), xwriter);
			xwriter.close();

			String result = swriter.toString();
			if (result.length() < limit || limit == -1) {
				builder.append(swriter.toString());
			} else {
				builder.append(swriter.toString().substring(0, limit));
			}

		} else {
			StringBuffer buffer = stringWriter.getBuffer();
			if (buffer.length() > limit) {
				builder.append(buffer.subSequence(0, limit));
			} else {
				builder.append(buffer);
			}
		}
	}

	/**
	 * Transform the string before display. The implementation in this class
	 * does nothing. Override this method if you wish to change the contents of
	 * the logged message before it is delivered to the output. For example, you
	 * can use this to mask out sensitive information.
	 * 
	 * @param originalLogString
	 *            the raw log message.
	 * @return transformed data
	 */
	protected String transform(String originalLogString) {
		return originalLogString;
	}

	protected void log(Logger logger, String message) {
		message = transform(message);
		if (writer != null) {
			writer.println(message);
			// Flushing the writer to make sure the message is written
			writer.flush();
		} else if (logger.getLevel().equals(Level.INFO)) {
			logger.log(Level.INFO, message);
		}
	}

	public void setShowBinaryContent(boolean showBinaryContent) {
		this.showBinaryContent = showBinaryContent;
	}

	public boolean isShowBinaryContent() {
		return showBinaryContent;
	}

	public boolean isBinaryContent(String contentType) {
		return contentType != null && BINARY_CONTENT_MEDIA_TYPES.contains(contentType);
	}

}
