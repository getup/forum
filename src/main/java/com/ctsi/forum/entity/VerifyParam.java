package com.ctsi.forum.entity;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

@JsonRootName("params")
@XmlRootElement(name = "params")
public class VerifyParam extends RequestParam{
	@NotNull(message="userId REQUIRED")
	private String userId;
	@NotNull(message="code REQUIRED")
	private String code;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	
	
	
}
