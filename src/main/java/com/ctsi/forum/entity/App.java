package com.ctsi.forum.entity;

public class App {
	private String app;
	private String secret;
	public App(String app,String secret) {
		this.app = app; 
		this.secret = secret;
	}
	public String getApp() {
		return app;
	}
	public void setApp(String app) {
		this.app = app;
	}
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}
	
}
