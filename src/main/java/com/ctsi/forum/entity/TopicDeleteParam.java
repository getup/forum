package com.ctsi.forum.entity;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

@JsonRootName("params")
@XmlRootElement(name = "params")
public class TopicDeleteParam extends RequestParam{
	@NotNull(message="topicId REQUIRED")
	private Integer topicId;

	public Integer getTopicId() {
		return topicId;
	}

	public void setTopicId(Integer topicId) {
		this.topicId = topicId;
	}
	
	
	
}
