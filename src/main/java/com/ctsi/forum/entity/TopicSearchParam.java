package com.ctsi.forum.entity;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

@JsonRootName("params")
@XmlRootElement(name = "params")
public class TopicSearchParam extends RequestParam{
	@NotNull(message="topicName")
	private String topicName;

	public String getTopicName() {
		return topicName;
	}

	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}



	
	
}
