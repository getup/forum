package com.ctsi.forum.entity;

public class Auth {

	private String app;
	private String timestamp;
	private String token;

	public boolean isValid(String secret) {
		this.token=this.token.replaceAll("\\s+", "");
		return TokenCipher.make(this.app, this.timestamp, secret).equals(
				this.token);
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	
}
