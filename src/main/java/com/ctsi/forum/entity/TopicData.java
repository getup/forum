package com.ctsi.forum.entity;

import java.util.List;

import com.ctsi.forum.dao.pojo.Topic;

public class TopicData  extends BasicData{
	List<Topic> topics=null;

	public List<Topic> getTopics() {
		return topics;
	}

	public void setTopics(List<Topic> topics) {
		this.topics = topics;
	}
	
	
	
}
