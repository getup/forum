package com.ctsi.forum.entity;

public class LoginData extends BasicData{
	private Integer mebId;
	private String mebName;
	private String mebType;
	private Integer point;
	private String ssoToken;
	public Integer getMebId() {
		return mebId;
	}
	public void setMebId(Integer mebId) {
		this.mebId = mebId;
	}
	public String getMebName() {
		return mebName==null?"":mebName;
	}
	public void setMebName(String mebName) {
		this.mebName = mebName;
	}
	public String getMebType() {
		return mebType==null?"":mebType;
	}
	public void setMebType(String mebType) {
		this.mebType = mebType;
	}
	public Integer getPoint() {
		return point;
	}
	public void setPoint(Integer point) {
		this.point = point;
	}
	public String getSsoToken() {
		return ssoToken==null?"":ssoToken;
	}
	public void setSsoToken(String ssoToken) {
		this.ssoToken = ssoToken;
	}
	
}
