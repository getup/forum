package com.ctsi.forum.entity;

import org.apache.commons.codec.digest.DigestUtils;

public final class TokenCipher {

	public static String make(String app, String timestamp, String seed) {
		return DigestUtils.md5Hex(app.concat(timestamp).concat(seed));
	}

}