package com.ctsi.forum.entity;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

@JsonRootName("params")
@XmlRootElement(name = "params")
public class UpdateParam extends RequestParam{
	@NotNull(message="topicId")
	   private Integer topicId;
	    private String topicName;

	    private Date planTime;

	    private Date completeTime;

	    private Integer topicStatus;

		public Integer getTopicId() {
			return topicId;
		}

		public void setTopicId(Integer topicId) {
			this.topicId = topicId;
		}

		public String getTopicName() {
			return topicName;
		}

		public void setTopicName(String topicName) {
			this.topicName = topicName;
		}

		public Date getPlanTime() {
			return planTime;
		}

		public void setPlanTime(Date planTime) {
			this.planTime = planTime;
		}

		public Date getCompleteTime() {
			return completeTime;
		}

		public void setCompleteTime(Date completeTime) {
			this.completeTime = completeTime;
		}

		public Integer getTopicStatus() {
			return topicStatus;
		}

		public void setTopicStatus(Integer topicStatus) {
			this.topicStatus = topicStatus;
		}



	
	
}
