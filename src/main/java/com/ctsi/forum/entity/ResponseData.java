package com.ctsi.forum.entity;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

@JsonRootName("data")
@XmlRootElement(name = "data")
public class ResponseData {
	private Integer code;
	private String message;
	private BasicData data;
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message==null?"":message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public BasicData getData() {
		return data;
	}
	public void setData(BasicData data) {
		this.data = data;
	}
	
}
