package com.ctsi.forum.entity;

public class RequestParam {
	protected Auth auth;

	public Auth getAuth() {
		return auth;
	}

	public void setAuth(Auth auth) {
		this.auth = auth;
	}
}
