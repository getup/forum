package com.ctsi.forum.entity;

import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.map.annotate.JsonRootName;

@JsonRootName("params")
@XmlRootElement(name = "params")
public class TopicParam extends RequestParam{
	@NotNull(message="topicName REQUIRED")
	private String topicName;
	@NotNull(message="planTime REQUIRED")
	private String planTime;
	@NotNull(message="completeTime REQUIRED")
	private String completeTime;
	public String getTopicName() {
		return topicName;
	}
	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}
	public String getPlanTime() {
		return planTime;
	}
	public void setPlanTime(String planTime) {
		this.planTime = planTime;
	}
	public String getCompleteTime() {
		return completeTime;
	}
	public void setCompleteTime(String completeTime) {
		this.completeTime = completeTime;
	}
	
	
	
}
