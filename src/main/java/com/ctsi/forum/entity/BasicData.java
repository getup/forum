package com.ctsi.forum.entity;

import javax.xml.bind.annotation.XmlSeeAlso;

@XmlSeeAlso({LoginData.class})
public class BasicData {
	protected Integer code;
	protected String message;
	public Integer getCode() {
		return code;
	}
	public void setCode(Integer code) {
		this.code = code;
	}
	public String getMessage() {
		return message==null?"":message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
