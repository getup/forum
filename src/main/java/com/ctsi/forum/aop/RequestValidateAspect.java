package com.ctsi.forum.aop;

import org.apache.log4j.Logger;

import com.ctsi.forum.constant.AppMap;
import com.ctsi.forum.constant.CusStatus;
import com.ctsi.forum.entity.App;
import com.ctsi.forum.entity.Auth;
import com.ctsi.forum.entity.RequestParam;
import com.ctsi.forum.exception.BizException;

public class RequestValidateAspect {
	public static Logger logger = Logger.getLogger(RequestValidateAspect.class);

	public void before(Object param) throws BizException {
		if (param instanceof RequestParam) {
			RequestParam rParam = (RequestParam) param;
			Auth auth = rParam.getAuth();
			try {
				if (auth == null) {
					logger.info("【实战项目】权限校验失败,未传入鉴权信息");
					throw new BizException(CusStatus.AUTH_FAILED);
				}
				App app = AppMap.getApp(auth.getApp());
				if (app == null) {
					logger.info("【实战项目】权限校验失败,鉴权信息无效，app[" + auth.getApp()
							+ "],timestamp[" + auth.getTimestamp() + "],token["
							+ auth.getToken() + "]");
					throw new BizException(CusStatus.AUTH_FAILED);
				}
				if (auth.isValid(app.getSecret())) {
					logger.info("【实战项目】权限校验失败,鉴权信息无效，app[" + auth.getApp()
							+ "],timestamp[" + auth.getTimestamp() + "],token["
							+ auth.getToken() + "]");
					throw new BizException(CusStatus.AUTH_FAILED);
				}
			} catch (Exception e) {
				logger.error("【实战项目】权限校验异常", e);
				throw new BizException(CusStatus.AUTH_FAILED);
			}
		}
	}
}
