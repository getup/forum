package com.ctsi.forum.constant;

public enum ServerCode {
	SUCESS(0, "成功"), AUTH_FAILED(-1, "认证失败"), SYSTEM_ERROR(-2, "系统异常"), OTHER_ERROR(
			-3, "其他");
	private Integer code;
	private String message;

	private ServerCode(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer code() {
		return code;
	}

	public String message() {
		return message;
	}
}
