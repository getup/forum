package com.ctsi.forum.constant;

import javax.ws.rs.core.Response.Status.Family;
import javax.ws.rs.core.Response.StatusType;

public enum CusStatus implements StatusType {

	SUCESS(0, "成功"),

	AUTH_FAILED(-1, "认证失败"),

	SYSTEM_ERROR(-2, "系统异常"),

	OTHER_ERROR(-3, "其他");


	private final int code;

	private final String reason;

	CusStatus(final int statusCode, final String reasonPhrase) {
		this.code = statusCode;
		this.reason = reasonPhrase;
	}

	public int getStatusCode() {
		return this.code;
	}

	public Family getFamily() {
		return Family.CLIENT_ERROR;
	}

	public String getReasonPhrase() {
		return this.reason;
	}

}
