package com.ctsi.forum.constant;

public enum LoginCode {
	SUCESS(0, "成功"), PARAM_FAIL(-1, "用户名或密码错误"),TOKEN_FAIL(-1,"ssoToken无效或已过期");
	private Integer code;
	private String message;

	private LoginCode(Integer code, String message) {
		this.code = code;
		this.message = message;
	}

	public Integer code() {
		return code;
	}

	public String message() {
		return message;
	}

}
