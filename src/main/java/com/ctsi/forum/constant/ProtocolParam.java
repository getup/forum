package com.ctsi.forum.constant;

public class ProtocolParam {

	public static final String TOKEN_HEADER = "X-AUTH-HEADER";

	public static final char TOKEN_SEPARATOR = ',';

	public static final String RESPONSE_STATUS = "X-RESPONSE-STATUS";

}
