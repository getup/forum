package com.ctsi.forum.constant;

import java.util.HashMap;
import java.util.Map;

import com.ctsi.forum.entity.App;

public class AppMap {
	private static Map<String,App> auths=new HashMap<String,App>();
	static{
		App testApp=new App("app1", "#$%");
		auths.put("app1",testApp);
		App mallApp=new App("forum", "sgd#4gh$%hre");
		auths.put("forum",mallApp);
	}
	public static App getApp(String app){
		return auths.get(app);
	}
}
