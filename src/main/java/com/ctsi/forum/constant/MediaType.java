package com.ctsi.forum.constant;

public final class MediaType {

	public static final String APPLICATION_XML = "application/xml; charset=UTF-8";

	public static final String APPLICATION_JSON = "application/json; charset=UTF-8";

}
