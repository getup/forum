package com.ctsi.forum.exception;

import com.ctsi.forum.constant.CusStatus;


public class BizException extends Exception {

	private static final long serialVersionUID = 1L;

	private final CusStatus status;

	public BizException() {
		this(CusStatus.SYSTEM_ERROR.getReasonPhrase(), CusStatus.SYSTEM_ERROR);
	}

	public BizException(final CusStatus status) {
		this(status.getReasonPhrase(), status);
	}

	public BizException(final String message) {
		this(message, CusStatus.SYSTEM_ERROR);
	}

	public BizException(final String message, final CusStatus status) {
		super(message);
		this.status = status;
	}

	public CusStatus getStatus() {
		return status;
	}

	public String toString() {
		return String.format("%s:%s", this.status, this.getMessage());
	}

	private BizException(String message, Throwable cause) {
		super(message, cause);
		this.status = CusStatus.SYSTEM_ERROR;
	}

	public static BizException fromString(String tuple) {
		try {
			int i = tuple.indexOf(":");
			CusStatus status = CusStatus.valueOf(tuple.substring(0, i));
			String message = tuple.substring(i + 1);
			return new BizException(message, status);
		} catch (Exception e) {
			return new BizException(tuple, e);
		}
	}

}
