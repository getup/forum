package com.ctsi.forum.provider.mapper;

import java.io.IOException;
import java.text.SimpleDateFormat;

import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.SerializerProvider;
import org.codehaus.jackson.map.annotate.JsonSerialize;

public class JaxrsJacksonJsonObjectMapper extends ObjectMapper {

	public JaxrsJacksonJsonObjectMapper() {
		super();
		super.configure(SerializationConfig.Feature.WRAP_ROOT_VALUE, true);
		super.configure(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE, true);
		super.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
		super.configure(SerializationConfig.Feature.WRITE_EMPTY_JSON_ARRAYS, true);
		super.configure(SerializationConfig.Feature.WRITE_NULL_MAP_VALUES, true);
		super.setSerializationInclusion(JsonSerialize.Inclusion.ALWAYS);
		super.getSerializerProvider().setNullValueSerializer(new JaxrsNullValJsonSerializer());
		this.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
	}

}

class JaxrsNullValJsonSerializer extends JsonSerializer<Object> {

	public void serialize(Object key, JsonGenerator jsonGenerator, SerializerProvider unused) throws IOException,
			JsonProcessingException {
//		jsonGenerator.writeNull();
		jsonGenerator.writeNumber("");
	}

}
