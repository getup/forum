package com.ctsi.forum.provider;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.ExceptionMapper;

import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonMappingException;

import com.ctsi.forum.constant.ServerCode;
import com.ctsi.forum.entity.ResponseData;

public class JsonParseExceptionMapper extends ExceptionProvider implements
		ExceptionMapper<JsonProcessingException> {

	@Override
	public Response toResponse(JsonProcessingException ex) {
		logger.info("【实战项目】请求参数解析异常[" + ex.getMessage() + "]");
		ResponseBuilder rb = Response.status(Response.Status.OK);
		ResponseData data = new ResponseData();
		data.setCode(ServerCode.OTHER_ERROR.code());
		data.setMessage("请求参数格式错误，无法解析");
		rb.entity(data);
		Response r = rb.build();
		return r;
	}

}
