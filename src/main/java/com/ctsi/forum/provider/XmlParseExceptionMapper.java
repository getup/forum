package com.ctsi.forum.provider;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.ExceptionMapper;

import com.ctc.wstx.exc.WstxUnexpectedCharException;
import com.ctsi.forum.constant.ServerCode;
import com.ctsi.forum.entity.ResponseData;

public class XmlParseExceptionMapper  extends ExceptionProvider implements
ExceptionMapper<BadRequestException> {

	@Override
	public Response toResponse(BadRequestException ex) {
		if (ex.getCause() instanceof WstxUnexpectedCharException) {
			logger.info("【实战项目】请求参数解析异常[" + ex.getMessage() + "]");
			ResponseBuilder rb = Response.status(Response.Status.OK);
			ResponseData data = new ResponseData();
			data.setCode(ServerCode.OTHER_ERROR.code());
			data.setMessage("请求参数格式错误，无法解析");
			rb.entity(data);
			Response r = rb.build();
			return r;
		}else{
			logger.error("【实战项目】请求未知异常",ex);
			ResponseBuilder rb = Response.status(Response.Status.OK);
			ResponseData data = new ResponseData();
			data.setCode(ServerCode.OTHER_ERROR.code());
			data.setMessage("请求系统异常");
			rb.entity(data);
			Response r = rb.build();
			return r;
		}
		
	}

}
