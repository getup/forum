package com.ctsi.forum.provider;

import java.io.EOFException;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.ws.rs.ext.ExceptionMapper;
import javax.xml.ws.soap.SOAPFaultException;

import com.ctsi.forum.constant.ServerCode;
import com.ctsi.forum.entity.ResponseData;

public class UnKnowExceptionMapper extends ExceptionProvider implements
		ExceptionMapper<Exception> {

	@Override
	public Response toResponse(Exception ex) {
		if(ex instanceof ClientErrorException){
			logger.error("【实战项目】请求方法不存在或请求为GET方式");
			ClientErrorException clientErrorException = (ClientErrorException) ex;
			return clientErrorException.getResponse();
		}
		//logger.error("【实战项目】请求未知异常["+ex.getMessage()+"]");
		ResponseBuilder rb = Response.status(Response.Status.OK);
		ResponseData data = new ResponseData();
		if(ex instanceof EOFException){
			logger.error("【实战项目】请求参数格式错误[EOFException]，无法解析");
			data.setCode(ServerCode.OTHER_ERROR.code());
			data.setMessage("请求参数格式错误，无法解析");
		}else if (ex instanceof SOAPFaultException) {
			logger.error("【实战项目】基础服务异常[SOAPFaultException]，message["+ex.getMessage()+"]");
			data.setCode(ServerCode.OTHER_ERROR.code());
			data.setMessage(ex.getMessage());
		}else{
			logger.error("【实战项目】请求未知异常["+ex.getMessage()+"]",ex);
			data.setCode(ServerCode.SYSTEM_ERROR.code());
			data.setMessage(ServerCode.SYSTEM_ERROR.message());
		}
		rb.entity(data);
		Response r = rb.build();
		return r;
	}

}
