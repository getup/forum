package com.ctsi.forum.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//import com.plateno.www.webservice.service.mebpoint.EPointState;
//import com.plateno.www.webservice.service.mebpoint.EnjoyPointInfo;
//import com.plateno.www.webservice.service.mebpoint.MebPointServiceSoap;
//import com.plateno.www.webservice.service.smsservice.SMSServiceSoap;
//import com.plateno.www.webservice.service.userinterface.MemberProperty;
//import com.plateno.www.webservice.service.userinterface.UserInterfaceSoap;















import com.ctsi.forum.constant.ServerCode;
import com.ctsi.forum.dao.mapper.TopicMapper;
import com.ctsi.forum.dao.pojo.Topic;
import com.ctsi.forum.dao.pojo.TopicExample;
import com.ctsi.forum.entity.TopicData;
import com.ctsi.forum.entity.TopicDeleteParam;
import com.ctsi.forum.entity.TopicParam;
import com.ctsi.forum.entity.TopicSearchParam;
import com.ctsi.forum.entity.UpdateParam;

@Service("topicService")
public class TopicService extends BaseService {
	
	@Autowired
	TopicMapper topicMapper;
	
	
	public TopicData addTopic(TopicParam param){
		logger.info("【实战项目】创建事项,topicName[" + param.getTopicName()+ "],completeTime["+ param.getCompleteTime()+  "],planTime["+ param.getPlanTime()+"]");
		TopicData data=new TopicData();
		Topic topic=new Topic();
		topic.setTopicName(param.getTopicName());
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
		try {
			topic.setCompleteTime(sdf.parse(param.getCompleteTime()));
			topic.setPlanTime(sdf.parse(param.getPlanTime()));
			topic.setTopicStatus(0);
			topicMapper.insert(topic);
			data.setCode(ServerCode.SUCESS.code());
			data.setMessage(ServerCode.SUCESS.message());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			logger.info("【实战项目】创建事项出错,topicName[" + param.getTopicName()+ "],completeTime["+ param.getCompleteTime()+  "],planTime["+ param.getPlanTime()+"]"+e);
		}
		return  data;
		
	}


	public TopicData deleteTopic(TopicDeleteParam param) {
		// TODO Auto-generated method stub
		logger.info("【实战项目】删除事项,topicId[" + param.getTopicId()+ "]");
		TopicData data=new TopicData();
		try {
			int flat=topicMapper.deleteByPrimaryKey(param.getTopicId());
			if(flat>0){
				data.setCode(ServerCode.SUCESS.code());
				data.setMessage(ServerCode.SUCESS.message());
				logger.info("【实战项目】删除事项成功,topicId[" + param.getTopicId()+ "]");
			}else{
				data.setCode(ServerCode.SYSTEM_ERROR.code());
				data.setMessage(ServerCode.SYSTEM_ERROR.message());
				logger.info("【实战项目】删除事项失败,topicId[" + param.getTopicId()+ "]");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("【实战项目】删除事项出错,topicId[" + param.getTopicId()+ "]"+e);
		}
		return null;
	}


	public TopicData searchTopic(TopicSearchParam param) {
		// TODO Auto-generated method stub
		logger.info("【实战项目】查询事项,topicId[" + param.getTopicName()+ "]");
		TopicData data=new TopicData();
		try {
			List<Topic> topics=null;
			TopicExample example=new TopicExample();
			example.or().andTopicNameEqualTo(param.getTopicName());
			topics=topicMapper.selectByExample(example);
			if(topics==null||topics.size()==0){
				topics=topicMapper.selectByExample(new TopicExample());
			}
				data.setCode(ServerCode.SUCESS.code());
				data.setMessage(ServerCode.SUCESS.message());
				logger.info("【实战项目】查询事项成功,topicId[" + param.getTopicName()+ "]");
			
			data.setTopics(topics);
			return data;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("【实战项目】查询事项出错,topicId[" + param.getTopicName()+ "]"+e);
		}
		return null;
	}


	public TopicData updateTopic(UpdateParam param) {
		// TODO Auto-generated method stub

		logger.info("【实战项目】更新事项,topicName[" + param.getTopicName()+ "]");
		TopicData data=new TopicData();
		try {
			Topic record=new Topic();
			record.setTopicId(param.getTopicId());
			record.setTopicName(param.getTopicName());
			record.setPlanTime(param.getPlanTime());
			record.setCompleteTime(param.getCompleteTime());
			record.setTopicStatus(param.getTopicStatus());
			int flag=topicMapper.updateByPrimaryKeySelective(record);
			if(flag>0){
				data.setCode(ServerCode.SUCESS.code());
				data.setMessage(ServerCode.SUCESS.message());
				logger.info("【实战项目】更新事项成功,topicName[" + param.getTopicName()+ "]");
			}else{
				data.setCode(ServerCode.SYSTEM_ERROR.code());
				data.setMessage(ServerCode.SYSTEM_ERROR.message());
				logger.info("【实战项目】更新事项成功,topicName[" + param.getTopicName()+ "]");
			}
			return data;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.info("【实战项目】查询事项出错,topicId[" + param.getTopicName()+ "]"+e);
		}
		return null;
	}

}
